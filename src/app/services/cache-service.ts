
import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse } from '@angular/common/http';

const MAXAGE = 30 * 1000;
const USER_LIST_URL = 'op=list';
const USER_LIST_AGE = 5 * 60 * 1000;

@Injectable()
export class RequestCacheService {
    cache = new Map();

    get(req: HttpRequest<any>): HttpResponse<any> | undefined {
        const url = req.urlWithParams;
        const cached = this.cache.get(url);
        const expireTime = this.getExpirationTime(url);

        if (!cached) {
            return undefined;
        }

        const isExpired = cached.lastRead < (Date.now() - expireTime);

        return isExpired ? undefined : cached.response;
    }

    set(req: HttpRequest<any>, response: HttpResponse<any>): void {
        const url = req.url;
        const entry = { url, response, lastRead: Date.now() };
        const expireTime = this.getExpirationTime(url);
        this.cache.set(url, entry);

        this.cache.forEach(expiredEntry => {
            if (expiredEntry.lastRead < expireTime) {
                this.cache.delete(expiredEntry.url);
            }
        });
    }

    private getExpirationTime(url: string) {
        if (url.indexOf(USER_LIST_URL) != -1)
            return USER_LIST_AGE;
        else
            return MAXAGE;
    }
}