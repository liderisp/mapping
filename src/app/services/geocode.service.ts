import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class GeocodeService {
  constructor(private http: HttpClient) { }
  
  getAddress(lon: number, lat: number) {
    return this.http.get('http://nominatim.openstreetmap.org/reverse?format=json&lon=' + lon + '&lat=' + lat);
  }
}