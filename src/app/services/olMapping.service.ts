import OlMap from 'ol/Map';
import Overlay from 'ol/Overlay';
import OlVectorSource from 'ol/source/Vector';
import OlVectorLayer from 'ol/layer/Vector';
import OlView from 'ol/View';
import OlFeature from 'ol/Feature';
import OlPoint from 'ol/geom/Point';
import OlXyzSource from 'ol/source/XYZ';
import OlTileLayer from 'ol/layer/Tile';
import { Icon, Style } from 'ol/style';

import { fromLonLat } from 'ol/proj';
import { Subject } from 'rxjs';
import { Vehicle } from '../contracts';

export class olMappingService {
  map: OlMap;
  vectorSource: OlVectorSource;
  vectorLayer: OlVectorLayer;
  xyzSource: OlXyzSource;
  tileLayer: OlTileLayer;
  view: OlView;
  displayValue: string;
  popup: Overlay;

  _selectedVehicles: Vehicle[];
  _activeVehicle: Vehicle | null;

  selectedVehicles: any = new Subject();
  activeVehicle: any = new Subject();

  constructor() {
    this.selectedVehicles.subscribe((vehicles: Vehicle[]) => {
      this.activeVehicle.next();

      if (this._selectedVehicles) {
        this._selectedVehicles.forEach((vehicle) => this.removeMarker(vehicle.vehicleid + ''));
      }

      this._selectedVehicles = vehicles;

      if (this._selectedVehicles) {
        this._selectedVehicles.forEach((vehicle) => this.addVehicle(vehicle));
      }
    });

    this.activeVehicle.subscribe((vehicle: Vehicle) => {
      this._activeVehicle = vehicle;
      this.setActiveMarker(vehicle ? vehicle.vehicleid + '' : '');
    })

  }

  createMap(mapContainerId: string) {
    this.vectorSource = new OlVectorSource();

    this.vectorLayer = new OlVectorLayer({
      source: this.vectorSource
    });

    this.xyzSource = new OlXyzSource({
      url: 'http://tile.osm.org/{z}/{x}/{y}.png'
    });

    this.tileLayer = new OlTileLayer({
      source: this.xyzSource
    });

    this.view = new OlView({
      center: fromLonLat([24.1052, 56.9496]),
      zoom: 14
    });

    this.map = new OlMap({
      target: mapContainerId,
      layers: [
        this.tileLayer,
        this.vectorLayer
      ],
      view: this.view
    });

    var self = this;

    this.map.on('singleclick', function (evt) {
      if (!self.popup) {
        return;
      }

      var feature = this.forEachFeatureAtPixel(evt.pixel,
        function (feature) {
          return feature;
        });
      if (feature) {
        var vehicleId = feature.getId();
        self.activeVehicle.next(self._selectedVehicles.find(vehicle => vehicle.vehicleid == vehicleId))

        self.showPopUp(vehicleId);

      } else {
        self.popup.setPosition();
      }
    });
  }

  addFeaturePopUp(popUpContainerId: string) {
    var element = document.getElementById(popUpContainerId);
    this.popup = new Overlay({
      element: element,
      positioning: 'bottom-center',
      offset: [5, -70],
      autoPan: true
    });

    this.map.addOverlay(this.popup);
  }


  addVehicle(vehicle: Vehicle) {
    if (vehicle.location)
      this.addMarker(vehicle.location.lon, vehicle.location.lat, vehicle.vehicleid + '', vehicle.vin, vehicle.color);
  }

  private addMarker(longitude: number, latitude: number, markerId: string, name: string, color: string) {
    var marker = new OlFeature({
      geometry: new OlPoint(fromLonLat([longitude, latitude])),
      name: name
    });

    marker.setStyle(new Style({
      image: new Icon(/** @type {module:ol/style/Icon~Options} */({
        color: color,
        crossOrigin: 'anonymous',
        src: '/assets/img/icon.png',
      }))
    }))

    // if id is set with properties it is not being set on libraries private variable "id_", there for some functions wont work properly
    marker.setId(markerId);

    this.vectorSource.addFeature(marker);
  }

  private removeMarker(markerId: string) {
    var markerToRemove = this.vectorSource.getFeatureById(markerId);
    if (markerToRemove) {
      this.vectorSource.removeFeature(markerToRemove);
    }
  }

  private setActiveMarker(markerId: string) {
    var marker = this.vectorSource.getFeatureById(markerId);

    this.showPopUp(markerId);
    if (marker) {
      this.view.setCenter(marker.getGeometry().getCoordinates());
    }
  }

  private showPopUp(markerId: string) {
    var marker = this.vectorSource.getFeatureById(markerId);
    var coordinates = marker ? marker.getGeometry().getCoordinates() : undefined;
    this.popup.setPosition(coordinates);
  }
}