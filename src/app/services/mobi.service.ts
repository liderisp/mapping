import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, VehicleLocation } from '../contracts';

@Injectable()
export class MobiService {
  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get<User[]>('http://mobi.connectedcar360.net/api/?op=list');
  }
  getLocations(userId: number) {
    return this.http.get<VehicleLocation>('http://mobi.connectedcar360.net/api/?op=getlocations&userid=' + userId);
  }
}