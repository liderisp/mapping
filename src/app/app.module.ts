import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ListViewComponent } from './components/list-view/list-view.component';
import { MapViewComponent } from './components/map-view/map-view.component';
import { olMappingService } from './services/olMapping.service';
import { MapArrowComponent } from './components/map-view/map-arrow/map-arrow.component';
import { GeocodeService } from './services/geocode.service';
import { RequestCacheService } from './services/cache-service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpCachingInterceptor } from './http-interceptors/http-caching-interceptor';
import { ErrorDialogComponent } from './components/error-dialog/error-dialog.component';
import { ErrorDialogService } from './services/error-dialog.service';
import { MobiService } from './services/mobi.service';

@NgModule({
  declarations: [
    AppComponent,
    ListViewComponent,
    MapViewComponent,
    MapArrowComponent,
    ErrorDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule
  ],
  providers: [
    olMappingService,
    MobiService,
    GeocodeService,
    RequestCacheService,
    ErrorDialogService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpCachingInterceptor, multi: true }
  ],
  entryComponents: [ErrorDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
