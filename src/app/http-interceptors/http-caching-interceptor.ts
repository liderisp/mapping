import { Injectable } from '@angular/core';
import { HttpEvent, HttpRequest, HttpResponse, HttpInterceptor, HttpHandler, HttpErrorResponse } from '@angular/common/http';

import { startWith, tap, catchError } from 'rxjs/operators';

import { Observable, of, throwError } from 'rxjs';
import { RequestCacheService } from '../services/cache-service';
import { Vehicle } from '../contracts';
import { ErrorDialogService } from '../services/error-dialog.service';


@Injectable()
export class HttpCachingInterceptor implements HttpInterceptor {
    constructor(private cache: RequestCacheService, private errorDialogService: ErrorDialogService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const cachedResponse = this.cache.get(req);
        return cachedResponse ? of(cachedResponse) : this.sendRequest(req, next, this.cache);
    }

    sendRequest(
        req: HttpRequest<any>,
        next: HttpHandler,
        cache: RequestCacheService): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            tap(event => {
                if (event instanceof HttpResponse) {
                    cache.set(req, event);
                }
            }),
            catchError((error: HttpErrorResponse) => {
                let data = {};
                data = {
                    reason: error && error.message ? error.message : '',
                    status: error.status
                };
                this.errorDialogService.openDialog(data);
                return throwError(error);
            })
        );
    }
}
