import { Component, OnInit, Input } from '@angular/core';
import { Vehicle } from 'src/app/contracts';
import { GeocodeService } from 'src/app/services/geocode.service';

@Component({
  selector: 'map-arrow',
  templateUrl: './map-arrow.component.html',
  styleUrls: ['./map-arrow.component.scss']
})
export class MapArrowComponent implements OnInit {
  _vehicle: Vehicle;

  @Input()
  set vehicle(vehicle: Vehicle) {
    this._vehicle = vehicle;

    if (vehicle != undefined) {
      if (this.vehicle && this.vehicle.location)
        this.getAddress(this.vehicle.location.lon, this.vehicle.location.lat);
    } else {
      this._vehicle = undefined;
      this.address = '';
    }
  }

  get vehicle() {
    return this._vehicle;
  }
  address: String;

  constructor(private geocodeService: GeocodeService) {


  }

  ngOnInit() {
  }

  getAddress(lon: number, lat: number) {
    this.geocodeService.getAddress(lon, lat).subscribe((data: any) => {
      this.address = data['display_name'];
    })
  }
}
