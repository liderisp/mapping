import { Component, OnInit } from '@angular/core';
import { olMappingService } from 'src/app/services/olMapping.service';
import { Vehicle } from 'src/app/contracts';

@Component({
  selector: 'map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})

export class MapViewComponent implements OnInit {
  activeVehicle: Vehicle;
  constructor(private mappingService: olMappingService) {
    this.mappingService.activeVehicle.subscribe((vehicle: Vehicle) => {
      this.activeVehicle = vehicle;
    })
  }

  ngOnInit() {
    this.mappingService.createMap("map");
  }
}