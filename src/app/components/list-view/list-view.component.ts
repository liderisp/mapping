import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { User, Vehicle } from 'src/app/contracts';
import { olMappingService } from 'src/app/services/olMapping.service';
import { timer } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { MobiService } from 'src/app/services/mobi.service';

const RELOAD_TIME = 60 * 1000;

@Component({
  selector: 'list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})

export class ListViewComponent implements OnInit {
  userList: User[] = [];
  columnsToDisplay = ['name', 'vehicleCount'];
  selectedUser: User | null;

  constructor(private mappingService: olMappingService,
    private mobiService: MobiService) { }

  ngOnInit() {
    this.mappingService.addFeaturePopUp("popup-container");

    this.getUserList();

    timer(RELOAD_TIME).subscribe(() => {
      if (this.mappingService._selectedVehicles) {
        this.loadVehicles();
      }
    });
  }

  setSelectedUser(user: User) {
    this.selectedUser = this.selectedUser === user ? null : user;

    this.loadVehicles();
  }

  setActiveVehicle(vehicle: Vehicle) {
    this.mappingService.activeVehicle.next(vehicle);
  }

  loadVehicles() {
    var vehicles = this.selectedUser ? this.selectedUser.vehicles : null;

    if (this.selectedUser) {
      this.getVehicleLocations(this.selectedUser.userid).subscribe((data) => {
        var response = data['data'].filter(value => Object.keys(value).length !== 0);

        vehicles.forEach((vehicle) => {
          vehicle.location = response.filter((location) => location.vehicleid == vehicle.vehicleid)[0];
        });
        
        this.mappingService.selectedVehicles.next(vehicles);
      })
    }
  }

  getUserList() {
    this.mobiService.getUsers().subscribe((data) => {
      this.userList = data['data'].filter(user => Object.keys(user).length !== 0);
    });
  }

  getVehicleLocations(userId: number) {
    return this.mobiService.getLocations(userId);
  }
}