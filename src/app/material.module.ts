import { NgModule } from '@angular/core';

import {
    MAT_FORM_FIELD_DEFAULT_OPTIONS,
    MatTableModule,
    MatRippleModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
} from '@angular/material';

@NgModule({
    imports: [
        MatTableModule,
        MatRippleModule,
        MatCardModule,
        MatDialogModule,
        MatButtonModule,
    ],
    exports: [
        MatTableModule,
        MatRippleModule,
        MatCardModule,
        MatDialogModule,
        MatButtonModule,
    ],
    providers: [
        {
            provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
            useValue: { appearance: 'outline' }
        }
    ]
})
export class MaterialModule { }