export interface Owner {
    name: string;
    surname: string;
    foto: string;
}

export interface User {
    userid: number;
    owner: Owner;
    vehicles: Vehicle[];
}
export interface VehicleLocation extends Location {
    vehicleid: number;
}

export interface Location {
    lat: number;
    lon: number;
}

export class Vehicle {
    vehicleid: number;
    make: string;
    model: string;
    year: string;
    color: string;
    vin: string;
    foto: string;
    location?: VehicleLocation;
}

